# wumpus-detectives

WumpusDetectives ist ein vollständiges Multi-Agenten-Framework für die
Simulation einer WumpusWorld.

## Die Agenten (wumpusDetective)

Die Agenten sind in Java implementiert und können sich untereinander über ein sehr
einfaches Protokoll (via TCP) Nachrichten schicken. Jeder Agent benötigt eine
installierte Java-Laufzeit-umgebung (JRE). Der Agent selbst ist eine JAR-Datei in
der bereits alle Abhängigkeiten verpackt sind.


[Dokumentation der Agenten](wumpusDetective/README.md)

## Der Monitor (wumpusMonitor)

Der Monitor generiert und verwaltet die Spielwelt. Er bietet eine Schnittstelle,
über die sich  Agenten registrieren, ihren Zustand melden und
Informationen zu ihrer aktuellen Position abrufen können.
Des Weiteren bietet der Monitor auch ein HTTP-Endpoint über den der gesamte Zustand der
Welt erfragt werden kann. Eine externe Software kann diese Daten
zur Visualisierung nutzen. Für jede Simulaion werden Parameter und Ergebnisse
in die Datei `results_benchmark.csv` zu späteren Auswertung gespeichert.


[Dokumentation des Monitors](wumpusMonitor/README.md)


## Visualisierung (wumpusVisualization)

Visualisiert die simulierte Welt.

[Dokumentation der Visualisierung](wumpusVisualization/README.md)


## Gemeinsame Bibliothek (wumpusCore)

*wumpusCore* bietet gemeinsam benutze Klassen und wird automatisch mit übersetzt.


## Übersetzen

Voraussetzung ist Java 11 und Apache Maven.

Mit folgendem Befehl lassen sich beide Projekte: *wumpusDetective* und
*wumpusMonitor* übersetzen.

```
mvn clean compile package
```

## Simulation und Ergebnisvisualisierung

Mithilfe des Shell-Skriptes `benchmark.sh` lassen sich Simulationsläufe
mit verschiedenen Parametern durchführen.
Zur Ausführung wird [GNU Parallel](https://www.gnu.org/software/parallel/)
benötigt, das sich um die nebenläufige Ausführung von Monitor und der Agenten kümmert.
GNU Parallel sollte in jeder gängigen Linux-Distribution als Paket vorliegen, meist
unter dem namen `parallel`.
Da für jeden Agenten eine eigene Java-VM gestartet wird, ist der Speicherbedarf
relativ hoch. Eine Simulation mit 32 Agenten benötigt auf einem System, ca. 14 GiB Speicher.
Die Agenten können bei bedarf auf verschiedene Systeme ausgelagert werden, sodass auch Simulationen
mit mehr als 100 Agenten möglich sind.


Das Python-Skript `plot_results.py` visualisiert die Simulationsergebnisse:

    plot_results.py results_benchmark.csv

![Steps](misc/result_steps.png)
![Reward](misc/result_rewards.png)
