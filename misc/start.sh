#!/bin/sh
HASH_FILE='/tmp/wumpus-hash-sum'
NEW_HASH_FILE='/tmp/new-wumpus-hash-sum'

find -iname "*.java" -type f -exec md5sum "{}" + > /tmp/new-wumpus-hash-sum
if [ -f "$HASH_FILE" ]; then
    if ! cmp -s "$HASH_FILE" "$NEW_HASH_FILE"; then
        mvn clean compile package
    fi
fi
mv "$NEW_HASH_FILE" "$HASH_FILE"
tmux new-session -d -n 'Wumpus Simulation' -s wumpus 'cd wumpusMonitor && ./start-monitor.sh'
tmux split-window -h
tmux send 'sleep 1 && cd wumpusDetective/test-scripts && ./dieter.sh' ENTER
# tmux split-window -v
# tmux send 'sleep 1 && cd wumpusDetective/test-scripts && ./klaus.sh' ENTER
tmux resize-pane -L 30
tmux a

