extends Camera2D

var screen_size

const ZOOM_MAX = Vector2(15, 15)
const ZOOM_MIN = Vector2(1, 1)
const ZOOM_DELTA = Vector2(1, 1)

# Called when the node enters the scene tree for the first time.
func _ready():
	screen_size = get_viewport_rect().size


func _process(delta):
	var speed = 4000
	var velocity = Vector2()  # The camera's movement vector.
	if Input.is_action_pressed("ui_right"):
		velocity.x += 1
	if Input.is_action_pressed("ui_left"):
		velocity.x -= 1
	if Input.is_action_pressed("ui_down"):
		velocity.y += 1
	if Input.is_action_pressed("ui_up"):
		velocity.y -= 1
	if velocity.length() > 0:
		velocity = velocity.normalized() * speed
		
	position += velocity * delta
	#position.x = clamp(position.x, 0, screen_size.x)
	#position.y = clamp(position.y, 0, screen_size.y)
	
	
	# zoom
	if Input.is_action_just_released("zoom_in") and zoom > ZOOM_MIN:
		#set_new_camera_position()
		zoom -= ZOOM_DELTA

	if Input.is_action_just_released("zoom_out") and zoom < ZOOM_MAX:
		#set_new_camera_position()
		zoom += ZOOM_DELTA
		
func set_new_camera_position():
	global_position = get_global_mouse_position()
