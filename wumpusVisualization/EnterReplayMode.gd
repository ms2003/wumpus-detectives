extends Button


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var tile_sets = get_parent().get_parent().get_parent().get_node("TileSets")
onready var start_replay_button = get_parent().get_node("StartReplayButton")
onready var pause_replay_button = get_parent().get_node("StopReplay")
onready var forward_button = get_parent().get_node("ForwardButton")
onready var rewind_button = get_parent().get_node("RewindButton")

var state = false

func _ready():
	self.connect("pressed", self, "button_pressed")
	state = false
	
func button_pressed():
	switch_mode()
	tile_sets.toggle_replay_mode()

func switch_mode():
	if state:
		self.text = "Enter Replay Mode"
	else:
		self.text = "Leave Replay Mode"
	start_replay_button.disabled = !start_replay_button.disabled
	pause_replay_button.disabled = !pause_replay_button.disabled
	forward_button.disabled = !forward_button.disabled
	rewind_button.disabled = !rewind_button.disabled
	state = !state
