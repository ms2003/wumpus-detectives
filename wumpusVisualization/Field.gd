extends Node2D


const EMPTY_TILE_ID = -1

# ground tiles
const GROUND_STATE_FREE_ID = 0
const GROUND_STATE_PIT_ID = 1
const GROUND_STATE_EXIT_ID = 2

const WALL_UP_L_BORDER_ID = 3
const WALL_UP_BORDER_ID = 4
const WALL_UP_R_BORDER_ID = 5
const WALL_LEFT_BORDER_ID = 6
const GROUND_STATE_ROCK_ID = 7
const WALL_RIGHT_BORDER_ID = 8
const WALL_BOTTOM_L_BORDER_ID = 9
const WALL_BOTTOM_BORDER_ID = 10
const WALL_BOTTOM_R_BORDER_ID = 11

# effect tiles
const GROUND_STATE_BREEZE_ID = 12
const GROUND_STATE_STENCH_ID = 13
const GROUND_STATE_GOLD_ID = 14
const GROUND_STATE_BREEZE_STENCH_ID = 15
const GROUND_STATE_BREEZE_GOLD_ID = 16
const GROUND_STATE_STENCH_GOLD_ID = 17
const GROUND_STATE_BREEZE_STENCH_GOLD_ID = 18

# agent tiles
const GROUND_STATE_AGENT_ID = 19
const GROUND_STATE_WUMPUS_ID = 20 

# fog of war tiles
const FOG_EXPLORED = 21
const FOG_UNEXPLORED = 22

var _coordinates : Vector2
var _ground_id : int
var _effect_id : int
var _agent_id : int
var _agent_list

func _init(tile_string_list, x, y):
	self._coordinates = Vector2(x,y)
	parse_field_from_tile_list(tile_string_list)

func _set_coordinates(x,y):
	self._coordinates = Vector2(x,y)
func _get_coordinates():
	return _coordinates
	
func _set_ground_id(ground_id):
	self._ground_id = ground_id
func _get_ground_id():
	return self._ground_id
	
func _set_effect_id(effect_id):
	self._effect_id = effect_id
func _get_effect_id():
	return self._effect_id

func _set_agent_id(agent_id):
	self._agent_id = agent_id
func _get_agent_id():
	return self._agent_id

func parse_field_from_tile_list(tile_string_list):
	var ground_tile_id
	if tile_string_list.has("PIT"):
		ground_tile_id = GROUND_STATE_PIT_ID
	elif tile_string_list.has("EXIT"):
		ground_tile_id = GROUND_STATE_EXIT_ID
	elif tile_string_list.has("ROCK"):
		ground_tile_id = GROUND_STATE_ROCK_ID
	else:
		ground_tile_id = GROUND_STATE_FREE_ID
	self._ground_id = ground_tile_id

	
	var effect_tile_id
	if tile_string_list.has("BREEZE") and tile_string_list.has("STENCH") and tile_string_list.has("GOLD"):
		effect_tile_id = GROUND_STATE_BREEZE_STENCH_GOLD_ID
	elif tile_string_list.has("BREEZE") and tile_string_list.has("STENCH"):
		effect_tile_id = GROUND_STATE_BREEZE_STENCH_ID
	elif tile_string_list.has("BREEZE") and tile_string_list.has("GOLD"):
		effect_tile_id = GROUND_STATE_BREEZE_GOLD_ID
	elif tile_string_list.has("STENCH") and tile_string_list.has("GOLD"):
		effect_tile_id = GROUND_STATE_STENCH_GOLD_ID
	elif tile_string_list.has("BREEZE"):
		effect_tile_id = GROUND_STATE_BREEZE_ID
	elif tile_string_list.has("STENCH"):
		effect_tile_id = GROUND_STATE_STENCH_ID
	elif tile_string_list.has("GOLD"):
		effect_tile_id = GROUND_STATE_GOLD_ID
	else:
		effect_tile_id = EMPTY_TILE_ID
	self._effect_id = effect_tile_id
	
	var agent_tile_id
	if tile_string_list.has("AGENT"):
		agent_tile_id = GROUND_STATE_AGENT_ID
	elif tile_string_list.has("WUMPUS"):
		agent_tile_id = GROUND_STATE_WUMPUS_ID
	else:
		agent_tile_id = EMPTY_TILE_ID
	self._agent_id = agent_tile_id
	
