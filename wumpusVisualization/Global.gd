extends Node


# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"
var child_process_ids = [] 

func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		for process_id in child_process_ids:
			OS.kill(process_id)
		get_tree().quit() # default behavior

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta: float) -> void:
#	pass
