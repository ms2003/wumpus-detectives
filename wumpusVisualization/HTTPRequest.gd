extends HTTPRequest


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	var _timer = Timer.new()
	add_child(_timer)
	_timer.connect("timeout", self, "_on_Timer_timeout")
	_timer.set_wait_time(5.0)
	_timer.set_one_shot(true) # Make sure it loops
	_timer.start()

func _on_Timer_timeout():
	self.cancel_request()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
