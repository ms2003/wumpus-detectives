extends Control


var _fog_tiles
var _tiles


# Called when the node enters the scene tree for the first time.
func _ready():
	_tiles = get_parent().get_node("TileSets")
	_fog_tiles = _tiles.get_node("FogOfWarTiles")

func _process(_delta):
	if Input.is_action_just_pressed("toggle_fog"):
		_fog_tiles.visible = !_fog_tiles.visible
		
	if Input.is_action_just_pressed("reset"):
		_tiles._reset()
		
