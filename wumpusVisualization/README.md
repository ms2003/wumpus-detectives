# wumpusVisualization

Eine auf der [Godot-Engine](https://godotengine.org/) basierende Applikation zur Anzeige einer laufenden
Simulation.

Die Applikation erfragt regelmäßig den Zustand des Monitors ab und visualisiert die simulierte
Welt. Derzeit erwartet die Applikation, dass die REST-API des Monitors unter
`http://127.0.0.1:12345` erreichbar ist.



## Starten von wumpusVisualization

Im Verzeichnis von wumpusVisualization einfach folgenden Befehl absetzen. Voraussetzung
ist natürlich eine Installation von Godot3.


```
godot3-runner
```


## Beispielausgabe mit 32 Agenten

![wumpusVis](media/vis_32_agents.webm)
