extends Node

var _replay_step_list

func _init():
	_replay_step_list = []
	
func _add_replay_step(step):
	self._replay_step_list.append(step)
	
func _get_replay_step_by_index(index : int):
	return _replay_step_list[index]
