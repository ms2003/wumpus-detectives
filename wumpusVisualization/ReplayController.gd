extends Node2D

var curr_step
onready var tile_sets = get_parent().get_parent().get_node("TileSets")

func step_back():
	curr_step = curr_step - 1
	
func step_forward():
	curr_step = curr_step + 1
	
func step_to_first():
	curr_step = 0
	
func step_to_last():
	curr_step = len(tile_sets._replay)
