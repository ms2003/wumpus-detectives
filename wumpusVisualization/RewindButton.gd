extends Button

onready var tile_sets = get_parent().get_parent().get_parent().get_node("TileSets")

func _ready():
	self.connect("pressed", self, "button_pressed")

func button_pressed():
	tile_sets.replay_step_back()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
