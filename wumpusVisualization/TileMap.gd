extends TileMap

var _tileset
var _timer = null

const GROUND_STATE_FREE_ID = 0
const GROUND_STATE_STENCH_ID = 1
const GROUND_STATE_EXIT_ID = 2
const GROUND_STATE_BREEZE_ID = 3
const GROUND_STATE_GOLD_ID = 4
const GROUND_STATE_PIT_ID = 5
const GROUND_STATE_AGENT_ID = 6
const GROUND_STATE_WUMPUS_ID = 7 

# Called when the node enters the scene tree for the first time.
func _ready():
	$HTTPRequest.connect("request_completed", self, "_on_request_completed")
	_tileset = get_tileset()
	_timer = Timer.new()
	add_child(_timer)

	_timer.connect("timeout", self, "_on_Timer_timeout")
	# _timer.set_wait_time(1.0)
	_timer.set_one_shot(false) # Make sure it loops
	_timer.start()

func _on_Timer_timeout():
	get_world_state()

func get_world_state():
	$HTTPRequest.request("http://127.0.0.1:12345/wumpus/worldstate")
	
func _on_request_completed(result, response_code, headers, body):
	var json = JSON.parse(body.get_string_from_utf8())
	if (json.result):
		setup_map(json.result)
	
func setup_map(json):
	var world = json.get("world")
	if world:
		if len(world) > 0:
			for y in range (len(world)):
				for x in range (len(world[y])):
					var tile_id = parse_tile(world[y][x].get("groundStates"))
					if tile_id != self.get_cell(x,y):
						self.set_cellv(Vector2(x,y), tile_id)
						print("replacing", x, y)
			draw_border(len(world[0]),len(world))

func parse_tile(tile_string_list):
	var tile_id
	if len(tile_string_list) == 2:
		if tile_string_list.has("BREEZE"):
			tile_id = GROUND_STATE_BREEZE_ID
		elif tile_string_list.has("STENCH"):
			tile_id = GROUND_STATE_STENCH_ID
		elif tile_string_list.has("GOLD"):
			tile_id = GROUND_STATE_STENCH_ID
		elif tile_string_list.has("PIT"):
			tile_id = GROUND_STATE_PIT_ID
		elif tile_string_list.has("AGENT"):
			tile_id = GROUND_STATE_AGENT_ID
		elif tile_string_list.has("WUMPUS"):
			tile_id = GROUND_STATE_WUMPUS_ID
		elif tile_string_list.has("EXIT"):
			tile_id = GROUND_STATE_EXIT_ID
		else: 
			tile_id = GROUND_STATE_FREE_ID
			
	elif len(tile_string_list) == 3:
		if tile_string_list.has("BREEZE") and tile_string_list.has("STENCH"):
			tile_id = GROUND_STATE_FREE_ID #TODO
		elif tile_string_list.has("BREEZE") and tile_string_list.has("GOLD"):
			tile_id = GROUND_STATE_FREE_ID #TODO
		elif tile_string_list.has("BREEZE") and tile_string_list.has("PIT"):
			tile_id = GROUND_STATE_FREE_ID #TODO
		elif tile_string_list.has("BREEZE") and tile_string_list.has("AGENT"):
			tile_id = GROUND_STATE_FREE_ID #TODO
			
	match tile_string_list:
		["FREE"]: tile_id = 0
		["FREE", "BREEZE"]: tile_id = 3
		["FREE", "PIT"]: tile_id = 5
		["FREE", "GOLD"]: tile_id = 4
		["FREE", "STENCH"]: tile_id = 1
		["FREE", "WUMPUS"]: tile_id = 7
		_: tile_id = GROUND_STATE_FREE_ID
	print (tile_id)
	return tile_id
	
func draw_border(grid_x, grid_y):
	_tileset = get_tileset()
	
	# upper and lower border:
	for x in grid_x:
		self.set_cellv(Vector2(x,-1), 11)
		self.set_cellv(Vector2(x, grid_y), 9)
		
	# left and right border:
	for y in grid_y:
		self.set_cellv(Vector2(-1, y), 12)
		self.set_cellv(Vector2(grid_x, y), 10)
	
	# corners:
	self.set_cellv(Vector2(-1,-1), 13)
	self.set_cellv(Vector2(-1, grid_y), 15)
	self.set_cellv(Vector2(grid_x, -1), 14)
	self.set_cellv(Vector2(grid_x, grid_y), 16)
	
