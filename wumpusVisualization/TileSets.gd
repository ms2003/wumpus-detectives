extends TileMap

var _tileset
var _timer = null
var _groundTiles 
var _effectTiles
var _agentTiles
var _fogOfWarTiles
var _camera
var _agentPositions = null
var _agents = null
var _loadingLabel
var _replay
var _replay_mode
var _stepLabel
var Field = load("res://Field.gd")
var ReplayStep = load("res://ReplayStep.gd")
var Replay = load("res://Replay.gd")
var curr_version = -1
var replay_step_count = 0
var replay_txt_array = []
var requestlist = []
var replay_timer
signal initial_setup(x, y)

onready var agentPrefab = preload("res://agentPrefab.tscn")
onready var httpRequestPrefab = preload("res://HTTPRequest.tscn")

const EMPTY_TILE_ID = -1
# ground tiles
const GROUND_STATE_FREE_ID = 0
const GROUND_STATE_PIT_ID = 1
const GROUND_STATE_EXIT_ID = 2

const WALL_UP_L_BORDER_ID = 3
const WALL_UP_BORDER_ID = 4
const WALL_UP_R_BORDER_ID = 5
const WALL_LEFT_BORDER_ID = 6
const WALL_RIGHT_BORDER_ID = 8
const WALL_BOTTOM_L_BORDER_ID = 9
const WALL_BOTTOM_BORDER_ID = 10
const WALL_BOTTOM_R_BORDER_ID = 11

# effect tiles
const GROUND_STATE_BREEZE_ID = 12
const GROUND_STATE_STENCH_ID = 13
const GROUND_STATE_GOLD_ID = 14
const GROUND_STATE_BREEZE_STENCH_ID = 15
const GROUND_STATE_BREEZE_GOLD_ID = 16
const GROUND_STATE_STENCH_GOLD_ID = 17
const GROUND_STATE_BREEZE_STENCH_GOLD_ID = 18

# agent tiles
const GROUND_STATE_AGENT_ID = 19
const GROUND_STATE_WUMPUS_ID = 20 

# fog of war tiles
const FOG_EXPLORED = 21
const FOG_UNEXPLORED = 22

# Called when the node enters the scene tree for the first time.
func _ready():
	_tileset = get_tileset()
	_groundTiles = get_node("GroundTiles")
	_effectTiles = get_node("EffectTiles")
	_agentTiles = get_node("AgentTiles")
	_fogOfWarTiles = get_node("FogOfWarTiles")
	_camera = get_parent().get_node("Camera2D")
	_loadingLabel = get_parent().get_node("LoadingCanvas/Loading_Label")
	
	_stepLabel = get_parent().get_node("ReplayCanvas/HBoxContainer/StepLabel")
	_groundTiles.tile_set = _tileset
	_effectTiles.tile_set = _tileset
	_agentTiles.tile_set = _tileset
	_fogOfWarTiles.tile_set = _tileset
	
	_timer = Timer.new()
	add_child(_timer)
	_reset()
	_timer.connect("timeout", self, "_on_Timer_timeout")
	_timer.set_wait_time(0.1)
	_timer.set_one_shot(false) # Make sure it loops
	_timer.start()
	
	replay_timer = Timer.new()
	add_child(replay_timer)
	replay_timer.connect("timeout", self, "_on_replay_timer_timeout")
	replay_timer.set_wait_time(0.5)
	replay_timer.set_one_shot(false) # Make sure it loops
	
	var file = File.new()
	file.open("res://replay.txt", file.WRITE)
	file.close()
	_replay_mode = false
# resets the application to the initial state

func _reset():
	replay_step_count = 0
	replay_txt_array = []
	curr_version = -1
	_stepLabel.text = "Version"
	_loadingLabel.visible = true
	
	_groundTiles.clear()
	_effectTiles.clear()
	_agentTiles.clear()
	_fogOfWarTiles.clear()

	for agent in get_tree().get_nodes_in_group("agent"):
		agent.queue_free()
	self.connect("initial_setup", self, "_on_initial_setup", [], CONNECT_ONESHOT)


# submits get_world_state to the ThreadPool
func _on_Timer_timeout():
	$ThreadPool.submit_task_unparameterized(self, "_get_world_state")


# requests the current world state from WumpusMonitor
func _get_world_state():
	var request = httpRequestPrefab.instance()
	requestlist.append(request)
	add_child(request)
	request.connect("request_completed", self, "_on_request_completed", [], CONNECT_ONESHOT)
	request.request("http://127.0.0.1:12345/wumpus/worldstate")


# is called when the world state request has been completed 
func _on_request_completed(result, _response_code, _headers, body):
	if _replay_mode == false:
		if result == HTTPRequest.RESULT_SUCCESS:
			var json = JSON.parse(body.get_string_from_utf8())
			if (json.result):
				setup_map(json.result, true)
	if len(requestlist) > 100:
		for request in requestlist:
			request.queue_free()
		requestlist.clear()


# setups the map and agents according to the current world state
func setup_map(json, store_replay):
	var version = json.get("version")
	if version > curr_version or !store_replay:
		_stepLabel.text = "Version " + String(version)
		_loadingLabel.visible = false
		var world = json.get("world")
		if world:
			if len(world) > 0:
				for y in range (len(world)):
					for x in range (len(world[y])):
						var field = Field.new(world[y][x].get("groundStates"), x, y)
						_groundTiles.set_cellv(field._get_coordinates(), field._get_ground_id())
						_effectTiles.set_cellv(field._get_coordinates(), field._get_effect_id())
						_agentTiles.set_cellv(field._get_coordinates(), field._get_agent_id())
						field.queue_free()

			var agent_coordinates = json.get("agentCoordinates")
			var alive_agents = json.get("agentAlive")
			var last_actions = json.get("lastAction")
			if !store_replay:
				initialize_agents(agent_coordinates)
			emit_signal("initial_setup", len(world[0]),len(world), agent_coordinates, store_replay)
			set_agent_positions(agent_coordinates, alive_agents, last_actions)
				 
		curr_version = version
		if store_replay:
			store_json_for_replay(json)


func set_agent_positions(agent_coordinates, alive_agents, last_actions):
	_agents = get_tree().get_nodes_in_group("agent")
	for agent in _agents:
		var agent_name = agent.get_agent_name()

		#set position
		if agent_coordinates.has(agent_name):
			var agent_position = Vector2(agent_coordinates.get(agent_name).get("x") +1 , agent_coordinates.get(agent_name).get("y")+1)
			agent.set_position(_agentTiles.map_to_world(Vector2(agent_position)))
		
			#set fog of war
			_fogOfWarTiles.set_cellv(Vector2(agent_coordinates.get(agent_name).get("x"), agent_coordinates.get(agent_name).get("y")), EMPTY_TILE_ID)
		
		#animate death or last action
		if last_actions.has(agent_name) and agent.active == true:
			var last_action = last_actions.get(agent_name)
			if last_action == "MOVE":
				agent.play("walk")
			elif last_action == "SHOOT":
				agent.play("shoot")
			elif last_action == "LEAVE":
				agent.active = false
				agent.play("leave")
				
		if alive_agents.has(agent_name) and agent.active == true:
			var agent_alive = alive_agents.get(agent_name)
			if agent_alive != true:
				agent.active = false
				agent.play("die")



# REPLAY UTILS
func init_replay():
	_reset()
	get_replay_txt()


func start_replay():
	replay_timer.start()


func toggle_replay_mode():
	_replay_mode = !_replay_mode
	if _replay_mode:
		init_replay()
	else:
		_reset()
		replay_timer.start()


func pause_replay():
	replay_timer.stop()


func replay_step_forward():
	pause_replay()
	if(replay_step_count < len(replay_txt_array)-2):
		setup_map_from_replay(replay_step_count+1)
		replay_step_count = replay_step_count +1


func replay_step_back():
	pause_replay()
	if(replay_step_count < len(replay_txt_array)-1 and replay_step_count >= 2):
		replay_step_count = replay_step_count -1
		setup_map_from_replay(replay_step_count-1)


func _on_replay_timer_timeout():
	if(replay_step_count < len(replay_txt_array)-1):
		setup_map_from_replay(replay_step_count)
		replay_step_count = replay_step_count + 1


func setup_map_from_replay(index):
	var json = str2var(replay_txt_array[index])
	setup_map(json, false)


func store_json_for_replay(json):
	var file = File.new()
	file.open("res://replay.txt", file.READ_WRITE)
	file.seek_end()
	file.store_line(var2str(json))
	file.store_line("~")
	file.close()


func get_replay_txt():
	var file = File.new()
	file.open("res://replay.txt", file.READ)
	var content = file.get_as_text()
	replay_txt_array = content.split("~", false)
	file.close()

# INITIAL SETUP UTILS

func _on_initial_setup(x,y, agent_coordinates, store_replay):
	draw_border(x,y)
	setup_camera(x,y)
	setup_fog_of_war(x,y)
	initialize_agents(agent_coordinates)
	if store_replay:
		var file = File.new()
		file.open("res://replay.txt", file.WRITE)
		file.close()


func initialize_agents(agent_coordinates):
	for agent in get_tree().get_nodes_in_group("agent"):
		agent.queue_free()
	var init_agents = agent_coordinates.keys()
	for agent in init_agents:
		var agent_instance = agentPrefab.instance()
		var label_text = ""
		label_text += agent
		agent_instance.get_child(0).set_text(label_text)
		agent_instance.set_agent_name(agent)
		agent_instance.add_to_group("agent")
		self.add_child(agent_instance)


func draw_border(grid_x, grid_y):
	# upper and lower border:
	for x in grid_x:
		_groundTiles.set_cellv(Vector2(x,-1), WALL_UP_BORDER_ID)
		_groundTiles.set_cellv(Vector2(x, grid_y), WALL_BOTTOM_BORDER_ID)
		
	# left and right border:
	for y in grid_y:
		_groundTiles.set_cellv(Vector2(-1, y), WALL_LEFT_BORDER_ID)
		_groundTiles.set_cellv(Vector2(grid_x, y), WALL_RIGHT_BORDER_ID)
	
	# corners:
	_groundTiles.set_cellv(Vector2(-1,-1), WALL_UP_L_BORDER_ID)
	_groundTiles.set_cellv(Vector2(-1, grid_y), WALL_BOTTOM_L_BORDER_ID)
	_groundTiles.set_cellv(Vector2(grid_x, -1), WALL_UP_R_BORDER_ID)
	_groundTiles.set_cellv(Vector2(grid_x, grid_y), WALL_BOTTOM_R_BORDER_ID)

	
func setup_fog_of_war(x_max,y_max):
	for y in range(y_max):
		for x in range(x_max):
			_fogOfWarTiles.set_cellv(Vector2(x,y), FOG_UNEXPLORED)


func setup_camera (x, y):
	var new_pos = Vector2((x+1)/2, (y+1)/2)
	_camera.set_position(_groundTiles.map_to_world(new_pos))
