extends AnimatedSprite


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var agent_name = ""
var active = true

# Called when the node enters the scene tree for the first time.
func _ready():
	self.connect("animation_finished", self, "_on_animation_finished")


func _on_animation_finished():
	if self.animation == "die":
		self.play("death")
		self.stop()
	if self.animation == "leave":
		self.stop()
		self.queue_free()
		
		
func set_agent_name(name):
	agent_name = name
func get_agent_name():
	return agent_name
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
