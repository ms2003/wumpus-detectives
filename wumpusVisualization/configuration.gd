extends CanvasLayer


var _r_size
var _s_size
var _n_size

var _r_agent
var _s_agent
var _n_agent

var _r_wumpus
var _s_wumpus
var _n_wumpus

var _r_pit
var _s_pit
var _n_pit

var _r_gold
var _s_gold
var _n_gold

var _r_rock
var _s_rock
var _n_rock

var _r_exit
var _s_exit
var _n_exit

var _r_radius
var _s_radius
var _n_radius

var _r_all
var _b_start
var path
var _loading
var _loading_text



# Called when the node enters the scene tree for the first time.
func _ready():
	
	var res = ProjectSettings.globalize_path("res://")
	path = res.trim_suffix("/wumpusVisualization/")
	#print(path)
	
	_r_size = get_node("size_reset")
	_s_size = get_node("size_slider")
	_n_size = get_node("size_number")
	
	_r_agent = get_node("agents_reset")
	_s_agent = get_node("agents_slider")
	_n_agent = get_node("agents_number")
	
	_r_wumpus = get_node("wumpus_reset")
	_s_wumpus = get_node("wumpus_slider")
	_n_wumpus = get_node("wumpus_number")
	
	_r_pit = get_node("pit_reset")
	_s_pit = get_node("pit_slider")
	_n_pit = get_node("pit_number")
	
	_r_gold = get_node("gold_reset")
	_s_gold = get_node("gold_slider")
	_n_gold = get_node("gold_number")
	
	_r_rock = get_node("rock_reset")
	_s_rock = get_node("rock_slider")
	_n_rock = get_node("rock_number")
	
	_r_exit = get_node("exit_reset")
	_s_exit = get_node("exit_slider")
	_n_exit = get_node("exit_number")
	
	_r_radius = get_node("radius_reset")
	_s_radius = get_node("radius_slider")
	_n_radius = get_node("radius_number")
	
	_r_all = get_node("reset_all")
	_b_start = get_node("start")
	_loading = get_node("loading")
	_loading_text = get_node("loading_text")
	
	
	_r_size.connect("button_up", self, "_reset_slider", [_s_size, _n_size, 32])
	_s_size.connect("value_changed", self, "_change_number", [_n_size])
	_n_size.connect("text_entered", self, "_change_slider", [_s_size])
	
	_r_agent.connect("button_up", self, "_reset_slider", [_s_agent, _n_agent, 1])
	_s_agent.connect("value_changed", self, "_change_number", [_n_agent])
	_n_agent.connect("text_entered", self, "_change_slider", [_s_agent])
	
	_r_wumpus.connect("button_up", self, "_reset_slider", [_s_wumpus, _n_wumpus, 95])
	_s_wumpus.connect("value_changed", self, "_change_number", [_n_wumpus])
	_n_wumpus.connect("text_entered", self, "_change_slider", [_s_wumpus])
	
	_r_pit.connect("button_up", self, "_reset_slider", [_s_pit, _n_pit, 95])
	_s_pit.connect("value_changed", self, "_change_number", [_n_pit])
	_n_pit.connect("text_entered", self, "_change_slider", [_s_pit])
	
	_r_gold.connect("button_up", self, "_reset_slider", [_s_gold, _n_gold, 75])
	_s_gold.connect("value_changed", self, "_change_number", [_n_gold])
	_n_gold.connect("text_entered", self, "_change_slider", [_s_gold])
	
	_r_rock.connect("button_up", self, "_reset_slider", [_s_rock, _n_rock, 65])
	_s_rock.connect("value_changed", self, "_change_number", [_n_rock])
	_n_rock.connect("text_entered", self, "_change_slider", [_s_rock])
	
	_r_exit.connect("button_up", self, "_reset_slider", [_s_exit, _n_exit, 10])
	_s_exit.connect("value_changed", self, "_change_number", [_n_exit])
	_n_exit.connect("text_entered", self, "_change_slider", [_s_exit])
	
	_r_radius.connect("button_up", self, "_reset_slider", [_s_radius, _n_radius, 0])
	_s_radius.connect("value_changed", self, "_change_number", [_n_radius])
	_n_radius.connect("text_entered", self, "_change_slider", [_s_radius])
	
	_r_all.connect("button_up", self, "_reset_all")
	_b_start.connect("button_up", self, "_start_simulation")


func _reset_slider(var _slider, var _number, var _default):
	_slider.set_value(_default);
	_number.set_text(String(_default))


func _change_number(var _change, var _number):
	_number.set_text(String(_change))


func _change_slider(var _change, var _slider):
	_slider.set_value(int(_change))


func _reset_all():
	_reset_slider(_s_size, _n_size, 32)
	_reset_slider(_s_agent, _n_agent, 4)
	_reset_slider(_s_wumpus, _n_wumpus, 95)
	_reset_slider(_s_pit, _n_pit, 95)
	_reset_slider(_s_gold, _n_gold, 75)
	_reset_slider(_s_rock, _n_rock, 65)
	_reset_slider(_s_exit, _n_exit, 10)
	_reset_slider(_s_radius, _n_radius, 0)

func _start_simulation():
	
	_loading.set_visible(true)
	_loading_text.set_visible(true)
	
	var t = Timer.new()
	t.set_wait_time(1)
	t.set_one_shot(false)
	self.add_child(t)
	t.start()
	
	var output = []
	
	_start_monitor()
	
	yield(t, "timeout")
	
	for i in _s_agent.get_value():
		print("Starte Agent Nr: " + String(i + 1))
		_start_agent(i)
		yield(t, "timeout")
	print("change scene")
	print(Global.child_process_ids)

	assert(get_tree().change_scene("res://main.tscn") == OK)

	for line in output:
		print(line)
		
		
func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		print(what)
		for process_id in Global.child_process_ids:
			OS.kill(process_id)
		get_tree().quit() # default behavior

func _start_monitor():
	print("Monitor started")
	var output = []
	
	var command_monitor = [
		"-jar",
		path + "/wumpusMonitor/target/wumpusMonitor-0.0.0-jar-with-dependencies.jar",
		"-b", "http://127.0.0.1:12345",
		"-w", "wumpus://localhost:6666", 
		"-l", "warn",
		"-s", _s_agent.get_value() + 6, 
		"-t", 300, 
		"-r", _s_radius.get_value(),
		"-he", _s_size.get_value(),
		"-wu", _s_wumpus.get_value(),
		"-pi", _s_pit.get_value(), 
		"-go", _s_gold.get_value(), 
		"-ro", _s_rock.get_value(),
		"-ex", _s_exit.get_value()
	]
	print(command_monitor)
	var process_id = OS.execute("java", command_monitor, false, output)
	Global.child_process_ids.append(process_id)


func _start_agent(agent_id):
	print("Agent started")
	var output = []
	var port = 10000
	var args = [
		"-jar",
		path + "/wumpusDetective/target/wumpusDetective-0.0.0-jar-with-dependencies.jar",
		"-l", "wumpus://localhost:" + String(port + agent_id),
		"-m", "wumpus://localhost:6666",
		"-n", "Agent-00" + String(agent_id + 1),
		"-v", "error", 
		"-he", _s_size.get_value()
	]
	var process_id = OS.execute("java", args, false, output)
	Global.child_process_ids.append(process_id)
